import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmpleosPage } from './empleos.page';

describe('EmpleosPage', () => {
  let component: EmpleosPage;
  let fixture: ComponentFixture<EmpleosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmpleosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

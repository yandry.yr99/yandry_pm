import { Component, OnInit } from '@angular/core';
import { Empleo } from '../../model/empleo';
import { EmpleosService } from '../../services/empleos.service';
import { Router } from '@angular/router';
import { Camera } from '@ionic-native/camera/ngx';
import { CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-crear-empleos',
  templateUrl: './crear-empleos.page.html',
  styleUrls: ['./crear-empleos.page.scss'],
})
export class CrearEmpleosPage implements OnInit {

  empleo: Empleo = new Empleo()
  base64Image: any

  constructor(private empleoService: EmpleosService,private camera: Camera,private router:Router){ }

  ngOnInit() {
  }

  crearEmpleo(){
    console.log(this.empleo)
    this.empleoService.saveEmpleado2(this.empleo)
    this.router.navigate([`empleos-list`])
  }

  saludar(data){
    console.log("saludar: ",data)
  }

  imagenCargada(e){
    console.log(JSON.stringify(e))
    this.empleo.image=e;
  }

}

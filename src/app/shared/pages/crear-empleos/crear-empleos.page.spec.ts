import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearEmpleosPage } from './crear-empleos.page';

describe('CrearEmpleosPage', () => {
  let component: CrearEmpleosPage;
  let fixture: ComponentFixture<CrearEmpleosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEmpleosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearEmpleosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Empleo } from '../../model/empleo';
import { EmpleosService } from '../../services/empleos.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editar-empleos',
  templateUrl: './editar-empleos.page.html',
  styleUrls: ['./editar-empleos.page.scss'],
})
export class EditarEmpleosPage implements OnInit {

  empleo: any
  empleoE: Empleo= new Empleo()

  constructor(private empleoService: EmpleosService,private route: ActivatedRoute,private router:Router) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id")
    let emepleo= await this.empleoService.getEmpleoById2(id)
    this.empleoE=emepleo;
  }

  editarEmpleo(){
    console.log(this.empleoE)
    this.empleoService.updateEmpleo(this.empleoE)
    this.router.navigate([`empleos-list`])
  }

}

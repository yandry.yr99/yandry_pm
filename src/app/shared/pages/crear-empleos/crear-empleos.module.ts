import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearEmpleosPageRoutingModule } from './crear-empleos-routing.module';

import { CrearEmpleosPage } from './crear-empleos.page';
import { ImageUploadComponent } from 'src/app/componentes/image-upload/image-upload.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearEmpleosPageRoutingModule
  ],
  declarations: [CrearEmpleosPage, ImageUploadComponent],
  exports: [ImageUploadComponent]
})
export class CrearEmpleosPageModule {}

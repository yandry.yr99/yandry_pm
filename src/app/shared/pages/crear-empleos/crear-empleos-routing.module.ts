import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearEmpleosPage } from './crear-empleos.page';

const routes: Routes = [
  {
    path: '',
    component: CrearEmpleosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearEmpleosPageRoutingModule {}

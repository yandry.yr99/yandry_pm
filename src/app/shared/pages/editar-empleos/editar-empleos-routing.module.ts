import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditarEmpleosPage } from './editar-empleos.page';

const routes: Routes = [
  {
    path: '',
    component: EditarEmpleosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditarEmpleosPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarEmpleosPageRoutingModule } from './editar-empleos-routing.module';

import { EditarEmpleosPage } from './editar-empleos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditarEmpleosPageRoutingModule
  ],
  declarations: [EditarEmpleosPage]
})
export class EditarEmpleosPageModule {}

import { Component, OnInit } from '@angular/core';
import { EmpleosService } from '../../services/empleos.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-lista-empleos',
  templateUrl: './lista-empleos.page.html',
  styleUrls: ['./lista-empleos.page.scss'],
})
export class ListaEmpleosPage implements OnInit {

  empleos: Observable<any[]>


  constructor(private empleosService: EmpleosService,private router: Router) { }

  ngOnInit() {
    this.empleos = this.empleosService.getEmpleos()
  }

  showEmpleo(id: any){
    this.router.navigate([`empleos/${id}`])
  }

  showCrearEmpleo(){
    this.router.navigate([`crear-empleos`])
  }

  showEditarEmpleo(id: any){
    this.router.navigate([`editar-empleos/${id}`])
  }

  trackByFn(index,obj){
    return obj.uid
  }

}

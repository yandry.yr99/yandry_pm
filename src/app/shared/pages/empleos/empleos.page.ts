import { Component, OnInit } from '@angular/core';
import { EmpleosService } from '../../services/empleos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-empleos',
  templateUrl: './empleos.page.html',
  styleUrls: ['./empleos.page.scss'],
})
export class EmpleosPage implements OnInit {

  empleo: any

  constructor(private empleadoService: EmpleosService,private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get("id")
    this.empleo=this.empleadoService.getEmpleo(id)
    let telefono= this.empleadoService.getTelefonos(id)
    telefono.subscribe(data=>{
      console.log(data)
    })
  }

}

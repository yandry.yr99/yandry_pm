import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'lista-empleos',
    loadChildren: () => import('./shared/pages/lista-empleos/lista-empleos.module').then( m => m.ListaEmpleosPageModule)
  },
  {
    path: 'empleos',
    loadChildren: () => import('./shared/pages/empleos/empleos.module').then( m => m.EmpleosPageModule)
  },
  {
    path: 'editar-empleos',
    loadChildren: () => import('./shared/pages/editar-empleos/editar-empleos.module').then( m => m.EditarEmpleosPageModule)
  },
  {
    path: 'crear-empleos',
    loadChildren: () => import('./shared/pages/crear-empleos/crear-empleos.module').then( m => m.CrearEmpleosPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

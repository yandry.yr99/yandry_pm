import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditarEmpleosPage } from './editar-empleos.page';

describe('EditarEmpleosPage', () => {
  let component: EditarEmpleosPage;
  let fixture: ComponentFixture<EditarEmpleosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarEmpleosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditarEmpleosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
